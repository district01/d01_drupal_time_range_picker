/**
 * d01_drupal_time_range_picker.
 */
(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.d01_drupal_time_range_picker = {
    attach: function (context, drupalSettings) {
      var componentSettings = drupalSettings.d01_drupal_time_range_picker || {};
      var wrapperClass = '.js-d01-drupal-trp';
      $(wrapperClass, context).once('initializeTimeRangePickers').each(function(i, obj) {
        var el = $(this);
        var elId = (el.attr('id')) ? el.attr('id') : false;
        if (elId) {
          var options = componentSettings[elId] || {};
          new D01DrupalTimeRangePicker(
            el,
            options
          ).init();
        }
      });
    }
  };
})(jQuery, Drupal);
