(function ($) {

  'use strict';

  /**
   * jQuery object
   * @external jQuery
   * @see {@link http://api.jquery.com/jQuery/}
   */

  /**
   * D01DrupalDateRangePicker
   *
   * @param {object} el
   *    a jQuery Dom element.
   * @param {object} timeRangeOptions
   *    an object containing all settings.
   * @constructor
   */
  var D01DrupalTimeRangePicker = function (el, timeRangeOptions) {
    var that = this;
    that.el = el;
    that.timeRangeOptions = timeRangeOptions || {};

    // Let user choose the displaying format for the date.
    // Fallback to default settings of plugin.
    that.displayFormat = that.timeRangeOptions['timeFormat'] || 'h:mm:ss p';
    that.displayTimezone = that.timeRangeOptions['timezoneDisplay'];

    // Get all inputs.
    that.fromInput = that.el.find('.js-d01-drupal-trp-from-input');
    that.untilInput = that.el.find('.js-d01-drupal-trp-until-input');
    that.fromHidden = that.el.find('.js-d01-drupal-trp-from');
    that.untilHidden = that.el.find('.js-d01-drupal-trp-until');

    // Handle minimum time.
    // Cast php minutes based timestamp
    // to javascript microsecond based timestamp.
    // By default we fallback to 00:00:00.
    that.minValue = that.timeRangeOptions['min'] || false;
    that.minValue = that.minValue ? moment(that.minValue * 1000) : false;
    that.minValue = that.minValue ? that.minValue.format(that.displayFormat) : moment().startOf('day').format(that.displayFormat);
    that.timeRangeOptions['minTime'] = that.minValue;

    // Handle maximum time.
    // Cast php minutes based timestamp
    // to javascript microsecond based timestamp.
    // By default we fallback to 23:59:59.
    that.maxValue = that.timeRangeOptions['max'] || false;
    that.maxValue = that.maxValue ? moment(that.maxValue * 1000) : false;
    that.maxValue = that.maxValue ? that.maxValue.format(that.displayFormat) : moment().endOf('day').format(that.displayFormat);
    that.timeRangeOptions['maxTime'] = that.maxValue;

    // Handle default from timestamp values.
    // Cast php minutes based timestamp
    // to javascript microsecond based timestamp.
    that.fromValue = that.timeRangeOptions['from'] || false;
    that.fromValue = that.fromValue ? moment(that.fromValue * 1000) : false;
    that.fromValue = that.fromValue ? that.fromValue.tz(that.displayTimezone) : false;

    // Handle default until timestamp values.
    // Cast php minutes based timestamp
    // to javascript microsecond based timestamp.
    that.untilValue = that.timeRangeOptions['until'] || false;
    that.untilValue = that.untilValue ? moment(that.untilValue * 1000) : false;
    that.untilValue = that.untilValue ? that.untilValue.tz(that.displayTimezone) : false;

  };

  /**
   * initializePickers().
   */
  D01DrupalTimeRangePicker.prototype.init = function () {
    var that = this;

    // Handle on change event for From input.
    var fromOptions  = {
      change: function() {

        // Get Moment object from Timepicker.
        var fromMoment = false;
        var fromTimeString = that.fromInput.timepicker().getTime();
        if (fromTimeString) {
          var fromDate = new Date(fromTimeString);

          fromMoment = moment().set({
            hour: fromDate.getHours(),
            minute: fromDate.getMinutes(),
            second: fromDate.getSeconds()
          });
        }

        // Get Moment object from Timepicker.
        var untilMoment = false;
        var untilTimeString = that.untilInput.timepicker().getTime();
        if (untilTimeString) {
          var untilDate = new Date(untilTimeString);

          untilMoment = moment().set({
            hour:   untilDate.getHours(),
            minute: untilDate.getMinutes(),
            second: untilDate.getSeconds()
          });
        }

        // When from moment is set check if until moment is still
        // valid, else set it to be the same as from moment.
        if (untilMoment && fromMoment && untilMoment < fromMoment) {
          that.untilInput.timepicker().setTime(fromMoment.format(that.displayFormat));
        }


        // Sync from moment to hidden input.
        if (fromMoment) {
          that.untilInput.timepicker('option', 'minTime', fromMoment.format(that.displayFormat));
          that.fromHidden.val(fromMoment.unix());
        } else {
          that.fromHidden.val('');
        }
      }
    };

    // Handle on change event for Until input.
    var untilOptions  = {
      change: function() {

        // Get Moment object from Timepicker.
        var fromMoment = false;
        var fromTimeString = that.fromInput.timepicker().getTime();
        if (untilTimeString) {
          var fromDate = new Date(fromTimeString);

          fromMoment = moment().set({
            hour: fromDate.getHours(),
            minute: fromDate.getMinutes(),
            second: fromDate.getSeconds()
          });
        }

        // Get Moment object from Timepicker.
        var untilMoment = false;
        var untilTimeString = that.untilInput.timepicker().getTime();
        if (untilTimeString) {
          var untilDate = new Date(untilTimeString);

          untilMoment = moment().set({
            hour:   untilDate.getHours(),
            minute: untilDate.getMinutes(),
            second: untilDate.getSeconds()
          });
        }

        // When until moment is set check if from moment is still
        // before until moment, else set it to be the same as until moment.
        if (untilMoment && fromMoment && fromMoment > untilMoment) {
          that.fromInput.timepicker().setTime(untilMoment.format(that.displayFormat));
        }

        // Sync from moment to hidden input.
        if (untilMoment) {
          that.untilHidden.val(untilMoment.unix());
        } else {
          that.untilHidden.val('');
        }
      }
    };

    // Combine specific settings with the default ones.
    $.extend(fromOptions, that.timeRangeOptions);
    $.extend(untilOptions, that.timeRangeOptions);

    // Initialize the timepickers.
    that.fromInput.timepicker(fromOptions);
    that.untilInput.timepicker(untilOptions);

    // Handle the default values.
    if (that.fromValue) {
      // Format to user defined display format.
      var fromFormattedTime = that.fromValue.format(that.displayFormat);

      // Sync values.
      that.fromInput.timepicker().setTime(fromFormattedTime);
    }

    if (that.untilValue) {

      // Format to user defined display format.
      var untilFormattedTime = that.untilValue.format(that.displayFormat);

      // Sync values.
      that.untilInput.timepicker().setTime(untilFormattedTime);
    }
  };

  /**
   * @type {D01DrupalTimeRangePicker}
   */
  window.D01DrupalTimeRangePicker = D01DrupalTimeRangePicker;

})(jQuery);
