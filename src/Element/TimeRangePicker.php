<?php

namespace Drupal\d01_drupal_time_range_picker\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Render\Element\CompositeFormElementTrait;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\d01_drupal_time\D01DrupalTime;

/**
 * A form element for time range picker.
 *
 * @RenderElement("d01_drupal_time_range_picker")
 */
class TimeRangePicker extends FormElement {

  use CompositeFormElementTrait;

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#process' => [
        [$class, 'processTimeRangePicker'],
      ],
      '#theme' => 'd01_drupal_time_range_picker',
      '#theme_wrappers' => [
        'form_element',
      ],
      '#min' => FALSE,
      '#max' => FALSE,
      '#timezone_display' => FALSE,
      '#js_settings' => [],
      '#attributes' => [
        'class' => [
          'js-d01-drupal-trp',
        ],
      ],
      '#attached' => [
        'library' => [
          'd01_drupal_time_range_picker/time_range_picker',
        ],
        'drupalSettings' => [
          'd01_drupal_time_range_picker' => [],
        ],
      ],
    ];
  }

  /**
   * Processes a pickadate form element.
   */
  public static function processTimeRangePicker(&$element, FormStateInterface $form_state, &$complete_form) {

    // Make sure we almost certain have a uniq id for every date picker.
    $unique_id = 'd01_drupal_trp_' . uniqid();

    $element['#tree'] = TRUE;

    $value = isset($element['#value']) && !empty($element['#value']) ? $element['#value'] : FALSE;

    // Set id and theming suggestions.
    $element['#attributes']['id'] = $unique_id;
    $element['#attributes']['data-twig-suggestions'] = ['d01_drupal_time_range_picker'];

    // Add the #js_settings keyed by id.
    $js_settings = is_array($element['#js_settings']) ? $element['#js_settings'] : [];
    $element['#attached']['drupalSettings']['d01_drupal_time_range_picker'][$unique_id] = $js_settings;

    // Set timezone or fallback to site timezone.
    $config = \Drupal::config('system.date');
    $timezone_display = $element['#timezone_display'] ? $element['#timezone_display'] : $config->get('timezone.default');
    $element['#attached']['drupalSettings']['d01_drupal_time_range_picker'][$unique_id]['timezoneDisplay'] = $timezone_display;

    // Enforce the use of a dropdown.
    $element['#attached']['drupalSettings']['d01_drupal_time_range_picker'][$unique_id]['dropdown'] = TRUE;

    // Enforce not to use dynamic.
    $element['#attached']['drupalSettings']['d01_drupal_time_range_picker'][$unique_id]['dynamic'] = FALSE;

    // Send min and max to javascript.
    $min = $element['#min'] && ($element['#min'] instanceof D01DrupalTime) ? $element['#min']->getTimestamp() : FALSE;
    $max = $element['#max'] && ($element['#max'] instanceof D01DrupalTime) ? $element['#max']->getTimestamp() : FALSE;
    $element['#attached']['drupalSettings']['d01_drupal_time_range_picker'][$unique_id]['min'] = $min;
    $element['#attached']['drupalSettings']['d01_drupal_time_range_picker'][$unique_id]['max'] = $max;

    // Get from timestamp.
    $from_value = FALSE;
    if ($value && isset($value['from']) && $value['from'] && ($value['from'] instanceof D01DrupalTime)) {
      $from_value = $value['from']->getTimestamp();
    }

    // Pass from value to javascript so we can build
    // the field responsible for displaying the time.
    $element['#attached']['drupalSettings']['d01_drupal_time_range_picker'][$unique_id]['from'] = $from_value;
    $element['from_input'] = [
      '#title' => t('From'),
      '#type' => 'textfield',
      '#attributes' => [
        'class' => [
          'js-d01-drupal-trp-from-input',
        ],
      ],
    ];

    // Set from timestamp to hidden field.
    $element['from'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => [
          'js-d01-drupal-trp-from',
        ],
      ],
      '#value' => $from_value,
    ];

    // Get until timestamp.
    $until_value = FALSE;
    if ($value && isset($value['until']) && $value['until'] && ($value['until'] instanceof D01DrupalTime)) {
      $until_value = $value['until']->getTimestamp();
    }

    // Pass until value to javascript so we can build
    // the field responsible for displaying the time.
    $element['#attached']['drupalSettings']['d01_drupal_time_range_picker'][$unique_id]['until'] = $until_value;
    $element['until_input'] = [
      '#title' => t('Until'),
      '#type' => 'textfield',
      '#attributes' => [
        'class' => [
          'js-d01-drupal-trp-until-input',
        ],
      ],
    ];

    // Set until timestamp to hidden field.
    $element['until'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => [
          'js-d01-drupal-trp-until',
        ],
      ],
      '#value' => $until_value,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input === FALSE) {
      $from = FALSE;
      if (isset($element['#default_value']) && isset($element['#default_value']['from']) && ($element['#default_value']['from'] instanceof D01DrupalTime)) {
        $from = $element['#default_value']['from'];
      }

      $until = FALSE;
      if (isset($element['#default_value']) && isset($element['#default_value']['until']) && ($element['#default_value']['until'] instanceof D01DrupalTime)) {
        $until = $element['#default_value']['until'];
      }

      return [
        'from' => $from,
        'until' => $until,
      ];
    }
    else {
      $from = FALSE;
      if (isset($input['from']) && !empty($input['from'])) {
        $from_date = DrupalDateTime::createFromTimestamp($input['from']);
        $from = new D01DrupalTime($from_date);
      }

      $until = FALSE;
      if (isset($input['until']) && !empty($input['until'])) {
        $until_date = DrupalDateTime::createFromTimestamp($input['until']);
        $until = new D01DrupalTime($until_date);
      }

      return [
        'from' => $from,
        'until' => $until,
      ];
    }
  }

}
